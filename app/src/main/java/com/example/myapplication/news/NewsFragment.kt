package com.example.myapplication.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.setBoldFont
import com.example.myapplication.setMedFont
import kotlinx.android.synthetic.main.frag_news.*

class NewsFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_news, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        frag_news_rv_1.layoutManager = LinearLayoutManager(requireContext()).apply {
            this.orientation = RecyclerView.HORIZONTAL
        }
        frag_news_rv_1.adapter = NewsAdapter(listOf<Int>(R.drawable.main_news_1_1,
            R.drawable.main_news_1_2,
            R.drawable.main_news_1_1,
            R.drawable.main_news_1_2))
        frag_news_rv_2.layoutManager = LinearLayoutManager(requireContext()).apply {
            this.orientation = RecyclerView.HORIZONTAL
        }
        frag_news_rv_2.adapter = NewsAdapter(listOf<Int>(R.drawable.main_news_2_1,
            R.drawable.main_news_2_2,
            R.drawable.main_news_2_1,
            R.drawable.main_news_2_2))
        frag_news_tv_title_1.setBoldFont()
        frag_news_et_search.setMedFont()
        frag_news_tv_title_2.setBoldFont()
    }
}