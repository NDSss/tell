package com.example.myapplication.news

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import kotlinx.android.synthetic.main.item_frag_main_rv.view.*

class NewsAdapter(val newsList: List<Int>): RecyclerView.Adapter<NewsAdapter.NewsHv1>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsHv1 =
        NewsHv1(LayoutInflater.from(parent.context).inflate(R.layout.item_frag_main_rv, parent, false))

    override fun getItemCount(): Int = newsList.size

    override fun onBindViewHolder(holder: NewsHv1, position: Int) {
        holder.itemView.item_frag_main_rv_item.setImageDrawable(ContextCompat.getDrawable(
            holder.itemView.context, newsList[position]
        ))
    }

    inner class NewsHv1(itemView: View) : RecyclerView.ViewHolder(itemView)
}