package com.example.myapplication.games

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import kotlinx.android.synthetic.main.item_frag_games.view.*
import kotlinx.android.synthetic.main.item_frag_main_rv.view.*

class GamesAdapter(val newsList: List<Int>): RecyclerView.Adapter<GamesAdapter.GamesVh>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GamesVh =
        GamesVh(LayoutInflater.from(parent.context).inflate(R.layout.item_frag_games, parent, false))

    override fun getItemCount(): Int = newsList.size

    override fun onBindViewHolder(holder: GamesVh, position: Int) {
        holder.itemView.item_frag_games_top.setImageDrawable(ContextCompat.getDrawable(
            holder.itemView.context, newsList[position]
        ))
    }

    inner class GamesVh(itemView: View) : RecyclerView.ViewHolder(itemView)
}