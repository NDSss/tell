package com.example.myapplication.games

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.achiv.AchivActivity
import com.example.myapplication.setBoldFont
import kotlinx.android.synthetic.main.frag_games.*

class GamesFragment : Fragment(){
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.frag_games, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        frag_games_rv_top.layoutManager = LinearLayoutManager(requireContext()).apply {
            this.orientation = RecyclerView.HORIZONTAL
        }
        frag_games_rv_top.adapter = GamesAdapter(
            listOf(
                R.drawable.bg_games_top_1,
                R.drawable.bg_games_top_2,
                R.drawable.bg_games_top_3,
                R.drawable.bg_games_top_1,
                R.drawable.bg_games_top_2,
                R.drawable.bg_games_top_3,
                R.drawable.bg_games_top_1,
                R.drawable.bg_games_top_2,
                R.drawable.bg_games_top_3
            )
        )
        frag_games_tv_title.setBoldFont()
        frag_games_tv_top.setBoldFont()
        frag_games_tv_today.setBoldFont()
        frag_games_tv_all.setBoldFont()
        frag_games_tv_all.setOnClickListener {
            startActivity(Intent(requireContext(), AchivActivity::class.java))
        }
    }
}