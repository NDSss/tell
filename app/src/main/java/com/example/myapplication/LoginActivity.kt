package com.example.myapplication

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.act_login.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_login)
        act_login_tv_title.setBoldFont()
        act_login_tv_reg.setBoldFont()
        act_login_tv_sub_title.setMedFont()
        act_login_tv_щк.setMedFont()
        act_login_btn_dobro.setMedFont()
        act_login_btn_dobro.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}

fun TextView.setBoldFont(){
    this.typeface = Typeface.createFromAsset(this.context.assets, "font/gothaprobol.otf" )
}

fun TextView.setMedFont(){
    this.typeface = Typeface.createFromAsset(this.context.assets, "font/gothapromed.otf" )
}

fun TextView.setRegFont(){
    this.typeface = Typeface.createFromAsset(this.context.assets, "font/gothaproreg.otf" )
}