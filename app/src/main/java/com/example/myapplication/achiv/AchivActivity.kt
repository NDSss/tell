package com.example.myapplication.achiv

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.example.myapplication.R
import com.example.myapplication.setBoldFont
import kotlinx.android.synthetic.main.act_achive.*

class AchivActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_achive)
        act_achive_tv_title.setBoldFont()
        act_achive_rv.layoutManager = GridLayoutManager(this, 2)
        act_achive_rv.adapter
        act_achive_rv.adapter = AchivementsAdapter(listOf<Int>(
            R.drawable.bg_achive_1,
            R.drawable.bg_achive_2,
            R.drawable.bg_achive_3,
            R.drawable.bg_achive_4,
            R.drawable.bg_achive_5))
        act_achive_iv_back.setOnClickListener {
            finish()
        }
    }
}