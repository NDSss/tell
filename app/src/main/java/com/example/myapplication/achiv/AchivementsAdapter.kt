package com.example.myapplication.achiv

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import kotlinx.android.synthetic.main.item_frag_games.view.*

class AchivementsAdapter(val newsList: List<Int>): RecyclerView.Adapter<AchivementsAdapter.AchivementsVh>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AchivementsVh =
        AchivementsVh(LayoutInflater.from(parent.context).inflate(R.layout.item_achivement, parent, false))

    override fun getItemCount(): Int = newsList.size

    override fun onBindViewHolder(holder: AchivementsVh, position: Int) {
        (holder.itemView as ImageView).setImageDrawable(ContextCompat.getDrawable(
            holder.itemView.context, newsList[position]
        ))
    }

    inner class AchivementsVh(itemView: View) : RecyclerView.ViewHolder(itemView)
}